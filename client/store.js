import { createStore, compose } from 'redux';
import { syncHistoryWithStore} from 'react-router-redux';
import { browserHistory } from 'react-router';
import rootReducer from './reducers/index';

const addLoggingToDispatch = (store) => {
    // save current dispatch method
    const rawDispatch = store.dispatch;
    // return the new dispatch with logging function, note that the old store.dispatch
    // receives an argument, the action, so we have it available here as well.
    // If console.group is not available, return the raw action:
    if (!console.group) {
        return rawDispatch;
    }
    return (action) => {
        // Group console statements (in Chrome)
        console.group(action.type);
        // print old state in grey (coloring clog statements works in Chrome)
        console.log('%c prev state', 'color: gray', store.getState());
        // log the action in blue
        console.log('%c action', 'color: blue', action);
        // save the return value
        const returnValue = rawDispatch(action);
        // log the new state (green), possible because it is a synchronous operation!
        console.log('%c prev state', 'color: green', store.getState());
        console.groupEnd(action.type);
        // return value from this function:
        return returnValue;
    };
};

const store = createStore(rootReducer);
// Wrap a logging mechanism around the store.dispatch (overwrite the functionality),
// never do this in production! We need a plugin for webpack for this to really work (L12 egghead)
if (process.env.NODE_ENV !== 'production') {
    store.dispatch = addLoggingToDispatch(store);
}

export const history = syncHistoryWithStore(browserHistory, store);

if(module.hot) {
  module.hot.accept('./reducers/',() => {
    const nextRootReducer = require('./reducers/index').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;
