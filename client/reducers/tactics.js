import Immutable from 'immutable';
import h from '../components/games/lib/tacticshelpers';

var initData = Immutable.fromJS({
    startTurn: true,
    gameTurn: true,
    gameFinished: false,
    player1: h.createPlayer('Player 1'),
    player2: h.createPlayer('Player 2'),
});

// a reducer takes in two things:
// 1. the action (info about what happened)
// 2. copy of current state
function tactics(state = initData, action) {

    // Note that since this is the tactics reducer, the state argument passed through is the tactics object itself.
    // See the CHANGE_TURN option below, we just update a first level prop of state, namely, gameTurn

    switch (action.type) {
        case 'CHANGE_TURN':
            return state.update('gameTurn', (turn) => !turn);
        case 'HIT_NUMBER':
            const active = action.active;
            let number = action.number;

            if (active) {

                const player = h.getPlayer(state, 'active');

                if (player.getIn(['score', number.toString(), 'hits']) < 3) {

                    // Get the keys of both players
                    const activePlayerKey = h.getPlayerKey(state, 'active');
                    const otherPlayerKey = h.getPlayerKey(state, 'other');

                    const gameFinished = h.gameFinished(state, number);
                    if (gameFinished.finished) {
                        return h.updateWinner(state, gameFinished, number);
                    } else if (player.getIn(['score', number.toString(), 'hits']) === 2) {
                        return state
                        .updateIn([activePlayerKey, 'score', number.toString(), 'hits'], (hits) => hits + 1)
                        .updateIn([otherPlayerKey, 'score', number.toString(), 'completed'], (value) => !value);
                    } else {
                        return state.updateIn([activePlayerKey, 'score', number.toString(), 'hits'], (hits) => hits + 1)
                    }

                } else {
                    // state not to be changed
                    return state;
                }

            } else {
                // wrong board clicked, state not to be changed
                return state;
            }


        case 'ADD_POINTS':
            console.log('ADD POINTS');
            number = action.number;
            var gameFinished = h.gameFinished(state, number);
            if (gameFinished.finished) {
                return h.updateWinner(state, gameFinished, number)
            } else {
                return state.updateIn([h.getPlayerKey(state, 'active'), 'score', number.toString(), 'hits'], (hits) => hits + 1)
            }

            return state;
        case 'RESET_GAME':
            console.log('RESET THE GAME');
            return state.update('startTurn', (value) => !value)
                        .update('gameTurn', (value) => !data.get('startTurn'))
                        .update('gameFinished', (value) => !value)
                        .updateIn(['player1', 'score'], (score) => Immutable.fromJS(h.getCleanScore()))
                        .updateIn(['player2', 'score'], (score) => Immutable.fromJS(h.getCleanScore()));
            return state;
        default:
            return state;
    }
}

export default tactics;
