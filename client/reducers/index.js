import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import tactics from './tactics';

const rootReducer = combineReducers({tactics, routing: routerReducer });

export default rootReducer;
