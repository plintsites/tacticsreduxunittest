import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
/*
    Header
    <Header />
 */

const Header = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        tagline: React.PropTypes.string.isRequired
    },

    render: function() {
        return (
            <header>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
                                <span className="sr-only">Toggle test</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#">{this.props.tagline}</a>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
                            <ul className="nav navbar-nav">
                                <li className="active"><a href="#">Home</a></li>
                                <li><a href="#">Settings</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        )
    }
});

export default Header;
