import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import Row from './Row';
import h from '../lib/tacticshelpers';
import _ from 'underscore';

/*
    PlayerField component: interface for a single player
    <PlayerField />
 */

const PlayerField = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        player: React.PropTypes.object.isRequired,
        turn: React.PropTypes.bool.isRequired,
        hitNumber: React.PropTypes.func.isRequired,
        addPoints: React.PropTypes.func.isRequired
    },

    renderRow: function(item) {
        return <Row key={item} number={item} hits={this.props.player.getIn(['score', item.toString(), 'hits'])} completedByOpponent={this.props.player.getIn(['score', item.toString(), 'completed'])} turn={this.props.turn} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
    },

    render: function() {
        var gameClass = 'gamefield';
        if (this.props.turn) {
            gameClass += ' turn';
        }
        /* Compute the points sofar for this player */
        var points = h.calculateScore(this.props.player.get('score'));

        return (
            <div className={gameClass}>
                <h2>{this.props.player.get('name')} (<span className="player-points">{points}</span>)</h2>
                {_.range(20,9,-1).map(this.renderRow)}
            </div>
        )
    }
});

export default PlayerField;
