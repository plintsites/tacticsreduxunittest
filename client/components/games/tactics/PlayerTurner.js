import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

/*
    PlayerTurner component: component to update turns
    <PlayerTurner />
 */

const PlayerTurner = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        changeTurn: React.PropTypes.func.isRequired
    },

    render() {
        // const fingerTap = require('./images/icon-24-one-finger-tap.png');
        return (
            <div className="turner-container" onClick={this.props.changeTurn}>
                <div className="turner">
                    {/*<img src="/build/css/images/icon-24-one-finger-tap.png"/>*/}
                    {/*<img src={fingerTap}/>*/}
                    <img src="https://www.plint-sites.nl/images/logo_plint-sites_klein.png" />
                </div>
            </div>
        )
    }
});

export default PlayerTurner;

