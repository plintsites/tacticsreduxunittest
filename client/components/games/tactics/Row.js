import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import PointsCounter from './PointsCounter';

/*
    Row component: to display a row with players progress in the game for a single number
    <Row />
 */

const Row = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        number: React.PropTypes.number.isRequired,
        hitNumber: React.PropTypes.func.isRequired,
        hits: React.PropTypes.number.isRequired,
        completedByOpponent: React.PropTypes.bool.isRequired,
        addPoints: React.PropTypes.func.isRequired,
        turn: React.PropTypes.bool.isRequired
    },

    render: function() {
        var cntrClass = 'tactics-row';
        var status = 'hidden';
        if (this.props.hits >= 3) {
            cntrClass += ' row-completed';
            status = '';
        }
        var itemClass = 'row-item hit-' + Math.min(3, this.props.hits);

        // console.log('number passed in props: ' + this.props.number);
        // console.log('turn passed in props:   ' + this.props.turn);

        return (
            <div className={cntrClass}>
                <span className={itemClass} data-item="1" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
                <span className={itemClass} data-item="2" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
                <span className={itemClass} data-item="3" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
                <PointsCounter amount={Math.max(0, this.props.number*(this.props.hits - 3))} status={status} completedByOpponent={this.props.completedByOpponent} number={this.props.number} addPoints={this.props.addPoints}/>
            </div>
        )
    }
});

export default Row;
