import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import Standings from './Standings';
import PlayerTurner from './PlayerTurner';
import PlayerField from './PlayerField';

/*
    TacticsGame component: is where the actual game will live
    <TacticsGame />
 */

const TacticsGame = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        player1: React.PropTypes.object.isRequired,
        player2: React.PropTypes.object.isRequired,
        turn: React.PropTypes.bool.isRequired,
        changeTurn: React.PropTypes.func.isRequired,
        hitNumber: React.PropTypes.func.isRequired,
        addPoints: React.PropTypes.func.isRequired
    },

    render: function() {
        return (
            <div className="tactics-container">
                <div className="gamefields">
                    <PlayerField player={this.props.player1} turn={this.props.turn} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
                    <PlayerTurner changeTurn={this.props.changeTurn}/>
                    <PlayerField player={this.props.player2} turn={!this.props.turn} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
                </div>
                <Standings wonPlayer1={this.props.player1.get('roundsWon')} wonPlayer2={this.props.player2.get('roundsWon')}/>
            </div>
        )
    }
});

export default TacticsGame;
