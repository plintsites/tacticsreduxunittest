// change turns
export const changeTurn = () => ({
  type: 'CHANGE_TURN'
});

export const hitNumber = (number, active) => ({
  type: 'HIT_NUMBER',
  number,
  active
});

export const addPoints = (number) => ({
  type: 'ADD_POINTS',
  number
});

export const resetGame = () => ({
  type: 'RESET_GAME'
});
