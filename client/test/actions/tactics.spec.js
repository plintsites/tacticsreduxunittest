import expect from 'expect';
import * as actions from '../../actions/actionCreators';

describe('actions', () => {
    it('should create an action to change player', () => {
        const expectedAction = {
            type: 'CHANGE_TURN'
        };
        expect(actions.changeTurn()).toEqual(expectedAction);
    });

    it('should create an action to mark a number hit', () => {
        const expectedAction = {
            type: 'HIT_NUMBER',
            number: 13,
            active: true
        };
        expect(actions.hitNumber(13, true)).toEqual(expectedAction);
    });

    it('should create an action to add points', () => {
        const expectedAction = {
            type: 'ADD_POINTS',
            number: 13
        };
        expect(actions.addPoints(13)).toEqual(expectedAction);
    });

    it('should create an action to reset the game', () => {
        const expectedAction = {
            type: 'RESET_GAME',
        };
        expect(actions.resetGame()).toEqual(expectedAction);
    });
});
