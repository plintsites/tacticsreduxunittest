import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import Standings from '../../components/games/tactics/Standings';
import expect from 'expect';

describe('Standings component', (t) => {
    it('The current standings is 3-2', () => {
        const p1 = 3;
        const p2 = 2;
        const result = shallow(<Standings wonPlayer1={p1} wonPlayer2={p2}/>);
        expect(result.find('h2').text()).toEqual('3 - 2');
    });
});
