import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import Row from '../../components/games/tactics/Row';
import expect from 'expect';

describe('Row component', () => {
    // SIMPLE TESTS
    // 1] Test that for a given number of hits the .row-items have the correct class
    // 2] Test that for a 3 or more hits the .row is complete
    // 3] Test that the Row component contains a <PointsCounter />
    // 4] Clicking a .row-item will execute the callback function

    const number=18, hits=2, completed=true, turn=true, amount=0, status='';
    const addPoints = () => {};
    const hitNumber = () => {};

    const result = shallow(<Row
        number={number}
        hits={hits}
        completedByOpponent={completed}
        turn={turn}
        hitNumber={hitNumber}
        addPoints={addPoints}
    />);

    it('With two hits, the row-items have a class hit-2', () => {
        expect(result.find('.row-item').first().hasClass('hit-2')).toExist();
    });

    it('The Row component contains a PointsCounter component', () => {
        const result = mount(<Row
            number={number}
            hits={hits}
            completedByOpponent={completed}
            turn={turn}
            hitNumber={hitNumber}
            addPoints={addPoints}
        />);

        expect(result.find('.points-container').hasClass('hidden')).toExist();
    });

    it('With three hits, the row is completed', () => {
        const hits = 3;
        const result = shallow(<Row
            number={number}
            hits={hits}
            completedByOpponent={completed}
            turn={turn}
            hitNumber={hitNumber}
            addPoints={addPoints}
        />);

        expect(result.hasClass('row-completed')).toExist();
    });

    it('Clicking a row-item calls the callback', () => {
        const hitNumber = function() {
            expect(15 > 3).toExist();
        };

        const result = mount(<Row
            number={number}
            hits={hits}
            completedByOpponent={completed}
            turn={turn}
            hitNumber={hitNumber}
            addPoints={addPoints}
        />);

        result.find('.row-item').first().simulate('click');
    });

});


