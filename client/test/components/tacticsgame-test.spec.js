import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import TacticsGame from '../../components/games/tactics/TacticsGame';
import h from '../../components/games/lib/tacticshelpers';
import Immutable from 'immutable';
import expect from 'expect';

describe('TacticsGame component', () => {

    const turn = true;
    const player1 = Immutable.fromJS(h.createPlayer('Lim'));
    const player2 = Immutable.fromJS(h.createPlayer('De Pan'));
    const changeTurn = () => {};
    const hitNumber = () => {};
    const addPoints = () => {};

    const result = shallow(<TacticsGame
        player1={player1}
        player2={player2}
        turn={turn}
        changeTurn={changeTurn}
        hitNumber={hitNumber}
        addPoints={addPoints}
    />);

    // 1] There is a div with class gamefields
    it('There is a div with class gamefields', () => {
        expect(result.find('.tactics-container').children().first().hasClass('gamefields')).toExist();
    });

});
