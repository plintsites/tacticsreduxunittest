import './setup';

import React from 'react';
import {shallow, mount} from 'enzyme';
import Immutable from 'immutable';
import expect from 'expect';

import h from '../../components/games/lib/tacticshelpers';
import th from './testhelpers';

import Finished from '../../components/games/tactics/Finished';


describe('Finished component', () => {

    // create a player instance
    const player1Tmp = h.createPlayer('lim');
    // overwrite the score => score = 55
    player1Tmp.score = th.getTestScore();
    // Make an immutable structure
    const player1 = Immutable.fromJS(player1Tmp);
    // create player2
    const player2 = Immutable.fromJS(h.createPlayer());
    // create callback function
    const resetGame = function(){};
    // shallow render it
    const result = shallow(<Finished player1={player1} player2={player2} resetGame={resetGame} />);

    // the actual tests
    it('The outermost div should have a class named overlay-container', () => {
        expect(result.hasClass('overlay-container')).toEqual(true);
    });

    it('The first player is called lim', () => {
        expect(result.find('.popup-standings .row').first().find('.col-md-4').first().text()).toEqual('lim');
    });

    it('The first player has exactly 37 points', () => {
        expect(parseInt(result.find('.popup-standings .row').first().find('.score').text()) === 37 ).toExist();
    });

});
