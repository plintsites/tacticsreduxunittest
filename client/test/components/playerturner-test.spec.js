import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import PlayerTurner from '../../components/games/tactics/PlayerTurner';
import expect from 'expect';

describe('PlayerTurner component', () => {

    const changeTurnEmpty = function() {};

    // shallow render the component
    const result = shallow(<PlayerTurner changeTurn={changeTurnEmpty} />);

    it('Contains an <img>', () => {
        const img = result.find('img');
        expect(img.length).toEqual(1);
    });

    it('Contains an <img> with src one-finger-tap', () => {
        const img = result.find('img');
        expect(result.find('img').prop('src').indexOf('logo_plint-sites_klein')).toBeGreaterThan(-1);
    });

    it('Clicking the PlayerTurner calls the given prop', () => {


        const changeTurn = function(){
            // Set a stupid assertion, this only returns a successfull test
            // if the function is called
            expect(15).toBeGreaterThan(3);
        };

        // mount component
        const resultdom = mount(<PlayerTurner changeTurn={changeTurn} />);

        resultdom.find('.turner-container').simulate('click');
    });
});
