import React from 'react';
import { shallow, mount } from 'enzyme';
import Header from '../../components/games/tactics/Header';
import expect from 'expect';

describe('Header component', () => {
    it('The active menu icon is Home', () => {
        const tagline = 'test';
        const header = shallow(<Header tagline={tagline}/>);

        expect(header.find('.nav.navbar-nav .active').text()).toEqual('Home')
    });
});

