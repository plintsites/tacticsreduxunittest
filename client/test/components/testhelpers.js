let testhelpers =  {

    /**
     * Used for testing => should go to some helper lib in test folder
     * @return {object} [description]
     */
    getFinishScore: function() {
        return {
            '20': {hits: 2, completed: false},
            '19': {hits: 3, completed: true},
            '18': {hits: 3, completed: true},
            '17': {hits: 3, completed: true},
            '16': {hits: 3, completed: true},
            '15': {hits: 3, completed: true},
            '14': {hits: 3, completed: true},
            '13': {hits: 3, completed: true},
            '12': {hits: 3, completed: true},
            '11': {hits: 3, completed: true},
            '10': {hits: 3, completed: true}
        };
    },

    /**
     * This function is used in the unit tests for setting up some score for a player
     * function should go to some helper lib in test folder
     * @return {object} [description]
     */
    getTestScore: function() {
        return {
            '20': {hits: 2, completed: false},
            '19': {hits: 4, completed: true},
            '18': {hits: 4, completed: true},
            '17': {hits: 3, completed: true},
            '16': {hits: 3, completed: true},
            '15': {hits: 3, completed: true},
            '14': {hits: 3, completed: true},
            '13': {hits: 3, completed: true},
            '12': {hits: 3, completed: true},
            '11': {hits: 3, completed: true},
            '10': {hits: 3, completed: true}
        };
    },


}

export default testhelpers;
