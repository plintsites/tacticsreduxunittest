import './setup';

import React from 'react';
import { shallow, mount } from 'enzyme';

import PointsCounter from '../../components/games/tactics/PointsCounter';

import expect from 'expect';

describe('PointsCounter component', (t) => {

    // Create the component by mounting it, than setup tests
    const amount = 54;
    const number = 18;
    const status = '';
    const statusHidden = 'hidden';
    const addPoints = function(){};
    const opponent1 = false;
    const opponent2 = true;

    const result1 = shallow(<PointsCounter number={number} amount={amount} status={status} completedByOpponent={opponent1} addPoints={addPoints} />);
    const result2 = shallow(<PointsCounter number={number} amount={amount} status={status} completedByOpponent={opponent2} addPoints={addPoints} />);
    const resultHidden = shallow(<PointsCounter number={number} amount={amount} status={statusHidden} completedByOpponent={opponent2} addPoints={addPoints} />);

    it('This player has 54 points', () => {
        expect(parseInt(result1.find('.points-amount').text())).toEqual(54);
    });

    it('The players opponent did not yet complete this number', () => {
        expect(result1.find('button > span').hasClass('glyphicon')).toExist();
    });

    it('The component is hidden', () => {
        expect(resultHidden.hasClass('hidden')).toExist();
    });

    it('Clicking the addPoints button calls the given prop', () => {

        const addPoints = function(){
            // Set a stupid assertion, this only returns a successfull test
            // if the function is called
            expect(15).toBeGreaterThan(3);
        };

        // mount component
        const resultdom = mount(<PointsCounter number={number} amount={amount} status={status} completedByOpponent={opponent1} addPoints={addPoints} />);

        resultdom.find('button').simulate('click');
    });
});
