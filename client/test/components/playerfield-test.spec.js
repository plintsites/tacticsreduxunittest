import './setup';
import React from 'react';
import { shallow, mount } from 'enzyme';
import PlayerField from '../../components/games/tactics/PlayerField';
import h from '../../components/games/lib/tacticshelpers';
import th from './testhelpers';
import Immutable from 'immutable';
import expect from 'expect';

describe('PlayerField component', (t) => {
    // SIMPLE TESTS
    // 1] A PlayerField contains 11 Rows
    // 2] An active PlayerField has a class 'turn'
    // 3] A PlayerField has a name
    // 4] A PlayerField has a score

    const turn = true;
    const player = Immutable.fromJS(h.createPlayer('Lim'));
    const hitNumber = () => {};
    const addPoints = () => {};

    const result = shallow(<PlayerField player={player} turn={turn} hitNumber={hitNumber} addPoints={addPoints}/>);
    const resultdom = mount(<PlayerField player={player} turn={turn} hitNumber={hitNumber} addPoints={addPoints}/>);

    it('A PlayerField contains 10 Rows', () => {
        expect(resultdom.find('.tactics-row').length).toEqual(11);
    });

    it('An active PlayerField has a class turn', () => {
        expect(result.hasClass('turn')).toExist();
    });

    it('An initial PlayerField has a name and score 0', () => {
        expect(result.find('h2').text()).toEqual('Lim (0)');
    });

    it('Further in the game there are points', () => {
        const playerTmp = h.createPlayer('Lim');
        playerTmp.score = th.getTestScore();
        // Make an immutable structure
        const player = Immutable.fromJS(playerTmp);

        const result = shallow(<PlayerField player={player} turn={turn} hitNumber={hitNumber} addPoints={addPoints}/>);

        expect(parseInt(result.find('.player-points').text())).toEqual(37);
    });

});

