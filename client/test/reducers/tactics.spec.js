import expect from 'expect';
import tactics from '../../reducers/tactics';
import * as actions from '../../actions/actionCreators';
import Immutable from 'immutable';
import deepFreeze from 'deep-freeze';
import h from '../../components/games/lib/tacticshelpers'

describe('Reducers', () => {
    describe('Action changTurn', () => {
        it('should change turn from p1 to p2', () => {

            // setup initial state
            const initState = Immutable.fromJS({
                gameTurn: true
            });

            // freeze the initial state
            deepFreeze(initState);

            // call the reducer with an initial state and an action
            const nextState = tactics(initState, actions.changeTurn());

            // make the assertion
            expect(nextState.toJS()).toEqual({gameTurn: false});
        });
    });

    describe('Action hitNumber', () => {
        it('Hitting the first will set number of hits to 1', () => {

            var initState = Immutable.fromJS({
                startTurn: true,
                gameTurn: true,
                gameFinished: false,
                player1: h.createPlayer('Player 1'),
                player2: h.createPlayer('Player 2'),
            });

            deepFreeze(initState);
            const nextState = tactics(initState, actions.hitNumber(20, true));
            expect(nextState.getIn(['player1', 'score', '20', 'hits'])).toEqual(1);

        });

        it('Hitting the third will mark the row complete', () => {
            var initState = Immutable.fromJS({
                startTurn: true,
                gameTurn: true,
                gameFinished: false,
                player1: h.createPlayer('Player 1'),
                player2: h.createPlayer('Player 2'),
            });

            deepFreeze(initState);
            const inter1 = tactics(initState, actions.hitNumber(20, true));
            const inter2 = tactics(inter1, actions.hitNumber(20, true));
            const nextState = tactics(inter2, actions.hitNumber(20, true));
            expect(nextState.getIn(['player1', 'score', '20', 'hits'])).toEqual(3);
            expect(nextState.getIn(['player2', 'score', '20', 'completed'])).toExist();
        });

        it('Hitting fourth time does nothing', () => {
            var initState = Immutable.fromJS({
                startTurn: true,
                gameTurn: true,
                gameFinished: false,
                player1: h.createPlayer('Player 1'),
                player2: h.createPlayer('Player 2'),
            });

            deepFreeze(initState);
            const inter1 = tactics(initState, actions.hitNumber(20, true));
            const inter2 = tactics(inter1, actions.hitNumber(20, true));
            const inter3 = tactics(inter2, actions.hitNumber(20, true));
            const nextState = tactics(inter3, actions.hitNumber(20, true));
            expect(nextState.getIn(['player1', 'score', '20', 'hits'])).toEqual(3);
            expect(nextState.getIn(['player2', 'score', '20', 'completed'])).toExist();
        });
    });
});
