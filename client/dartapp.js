import React from 'react';

import { render } from 'react-dom';

// Import css
import css from './assets/app.less';

// Import Components
import App from './components/App';
import StartScreen from './components/StartScreen';
import Tactics from './components/games/tactics/Tactics';
import Leg501 from './components/games/leg501/Leg501';

// import react router deps
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

// import Provider to make components aware of the store
import { Provider } from 'react-redux';
// import Store from Redux
import store, { history } from './store';

const router = (
	<Provider store={store}>
	  	<Router history={browserHistory}>
	    	<Route path="/" component={App}>
				<IndexRoute component={StartScreen}></IndexRoute>
				<Route path="/tactics" component={Tactics}></Route>
				<Route path="/leg501" component={Leg501}></Route>
	    	</Route>
	  	</Router>
	</Provider>
)

console.log('just before render');

render(router, document.getElementById('root'));
